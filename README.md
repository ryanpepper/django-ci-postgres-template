# django-ci-postgres-template

An example project which has GitLab CI set up for a Django project, runs the unit tests and 
processes the results of failed tests using the outputted JUnit XML for pull requests.

The main body is in the .gitlab-ci.yml file.
