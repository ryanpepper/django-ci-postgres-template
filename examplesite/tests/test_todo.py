import pytest
from examplesite.models import Todo


@pytest.mark.django_db
def test_create_Todo():
    Todo.objects.create(todo="My text")
    assert Todo.objects.all()[0].todo == "My text"
