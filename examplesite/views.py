from django.views.generic import ListView
from examplesite.models import Todo


class TodosView(ListView):
    model = Todo
    template_name = 'index.html'
